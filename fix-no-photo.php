<?php

echo "Clearing no-photo images...\n";

$noPhotoJpg = './base-img/no-photo.jpg';

$logFile = __DIR__ . '/no-photo.log';
if (is_writable($logFile)) {

	if (isset($options['dry-run'])) {

		$result = l_mysql_query("SELECT obj_id AS object, field_id AS field  FROM `cms3_object_content` WHERE `text_val` = '$noPhotoJpg'");

		while ($row = mysql_fetch_assoc($result)) {
			echo $style("\033[0;41m") . "Row founded $row[object],$row[field]" . $style("\033[0m") . "\n";

			file_put_contents($logFile, "$options[name],$row[object],$row[field]\n", FILE_APPEND);
		}
		
	} else {
		// Очищаем строгое вхождение
		l_mysql_query("UPDATE `cms3_object_content` SET `text_val` = '' WHERE `text_val` = '$noPhotoJpg'");
	}


	// Логируем другие
	$result = l_mysql_query("SELECT obj_id AS object, field_id AS field  FROM `cms3_object_content` WHERE `text_val` LIKE '%no-photo%' OR `varchar_val` LIKE '%no-photo%'");


	while ($row = mysql_fetch_assoc($result)) {
		echo $style("\033[0;41m") . "Row founded $row[object],$row[field]" . $style("\033[0m") . "\n";

		file_put_contents($logFile, "$options[name],$row[object],$row[field]\n", FILE_APPEND);
	}
} else {
	echo $style("\033[0;41m") . "Log does not writable: $logFile!" . $style("\033[0m") . "\n";
}
