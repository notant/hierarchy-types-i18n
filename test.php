<?php

$pages = new selector('pages');
$pages->where('is_active')->equals(array(0, 1)); // Выбираем все страницы, по умолчанию только активные.
$pages->option('return', array('id')); // Загружаем только ID шники страниц, объекты создаёт потом.

echo "Site $options[name] has " . $pages->length() . " pages.\n";
