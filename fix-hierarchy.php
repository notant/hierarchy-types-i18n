<?php

$not_like = 'i18n::%';

$log_file = __DIR__ . '/hierarchy-type.log';

if (is_writable($log_file)) {

	if (isset($options['dry-run'])) {

		$result = l_mysql_query("SELECT `name`, `ext` FROM `cms3_hierarchy_types` WHERE `title` NOT LIKE '{$not_like}'");

		while ($row = mysql_fetch_assoc($result)) {
			echo $style("\033[0;41m") . "Row founded $row[name], $row[ext]" . $style("\033[0m") . "\n";

			file_put_contents($log_file, "$options[name],$row[name],$row[ext]\n", FILE_APPEND);
		}

	} else {
		$result = l_mysql_query("SELECT `id`, `name`, `ext` FROM `cms3_hierarchy_types` WHERE `title` NOT LIKE '{$not_like}'");
		while ($row = mysql_fetch_assoc($result)) {
			$hierarchy_name = "i18n::hierarchy-type-{$row['name']}-{$row['ext']}";

			echo "I found - {$row['name']}, {$row['ext']} \n";

			// update fields.
			l_mysql_query("UPDATE `cms3_hierarchy_types` SET `title` = '{$hierarchy_name}' WHERE `title` NOT LIKE '{$not_like}' AND `id` = '{$row['id']}'");
		}
	}

	// Логируем другие
	$result = l_mysql_query("SELECT `name`, `ext`, `title` FROM `cms3_hierarchy_types` WHERE `title` NOT LIKE '{$not_like}'");

	echo "Did i found any rows, that wasn't changed? \n";
	while ($row = mysql_fetch_assoc($result)) {
		echo $style("\033[0;41m") . "Row founded $row[name],$row[ext],$row[title]" . $style("\033[0m") . "\n";

		file_put_contents($logFile, "$options[name],$row[name],$row[ext],$row[title]\n", FILE_APPEND);
	}
	if (mysql_num_rows($result) == 0) echo "Nope!";

} else {
	echo $style("\033[0;41m") . "Log does not writable: $log_file!" . $style("\033[0m") . "\n";
}